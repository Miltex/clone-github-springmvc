<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>JSP Redirect Example</title>
</head>
<body>
	<% response.sendRedirect(request.getContextPath() + "/customer/list"); %>
	Teste bem vindo!!! index.jsp<a href="<c:url value="/logout" />">Logout </a><a href="${pageContext.request.contextPath}/logout?${_csrf.parameterName}=${_csrf.token}"> Sairfora</a>
</body>
</html>
