<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Spring MVC 5 + Hibernate 5 + JSP + MySQL</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-3.3.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

</head>
<body>
	<sec:authentication var="principal" property="principal" />
	<div class="container">
		<div class="col-md-offset-2 col-md-7">
			<div class="text-primary" align="right">Bem vindo: ${principal.username}</div>
			<h2>Administrar Gest�o de Contatos</h2><div align="right"><a href="<c:url value="/logout" />">Sair</a></div>
			<div><a href="<c:url value="/customer/list" />">Voltar</a></div>
			<hr />
		</div>
	</div>
</body>
</html>
