<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Gest�o de Clientes</title>

<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-3.3.1.min.js" />"></script>
<script src="<c:url value="/resources/js/popper.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap-confirmation.min.js" />"></script>


</head>
<body
	onload="$('[data-toggle=confirmation]').confirmation({rootSelector: '[data-toggle=confirmation]',});">
	
	<sec:authentication var="principal" property="principal" />
	
	<div class="container">
		<div class="col-md-offset-1 col-md-10">
			
			<div class="text-primary" align="right">Bem vindo: ${principal.username}</div>
			<h2>Gest�o de Contatos</h2><div><a href="<c:url value="/logout" />">Sair</a></div>
			<sec:authorize access="hasAnyRole('ROLE_ADMIN')">
    			<div align="right"><a href="<c:url value="/admin" />">Administrador</a></div>
			</sec:authorize>
			
			<hr />
			<div align="right"><a href="<c:url value="/customer/list" />">Voltar</a></div>	
			<br />
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title text-center bg-primary text-uppercase font-weight-bold">Estados</div>
				</div>
				<div class="panel-body">
					<table class="table table-striped table-bordered">

						<!-- loop over and print our customers -->
						<c:forEach var="tempCustomer" items="${estados}">

							<tr>
								<td>${tempCustomer.isocode}</td>

							</tr>

						</c:forEach>

					</table>

				</div>
			</div>
		</div>

	</div>
</body>
</html>