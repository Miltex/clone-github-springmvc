<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Spring MVC 5 + Hibernate 5 + JSP + MySQL</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-3.3.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

</head>
<body>
	<sec:authentication var="principal" property="principal" />
	<div class="container">
		<div class="col-md-offset-2 col-md-7">
			<div class="text-primary" align="right">Bem vindo: ${principal.username}</div>
			<h2>Gest�o de Contatos</h2><div><a href="<c:url value="/logout" />">Sair</a></div>
			<hr />
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">Dados do Cliente</div>
				</div>
				<div class="panel-body">
					<form:form action="saveCustomer" cssClass="form-horizontal"
						method="post" modelAttribute="customer">

						<c:url var="listagem" value="/customer/list"/>

						<!-- need to associate this data with customer id -->
						<form:hidden path="id" />

						<div
							class="form-group ${status.error ? 'form-control is-invalid' : ''}  ">
							<label for="firstname" class="col-md-3 control-label">Nome</label>
							<div class="col-md-9 ">
								<form:input path="firstName" cssClass="form-control" />
								<form:errors path="firstName" cssClass="text-danger" />
							</div>
						</div>
						<div
							class="form-group ${errors.hasFieldErrors('lastName') ? 'has-error':'teste'} ">
							<label for="lastname" class="col-md-3 control-label">Sobrenome</label>
							<div class="col-md-9">
								<form:input path="lastName" cssClass="form-control" />
								<form:errors path="lastName" cssClass="text-danger" />

							</div>
						</div>

						<div class="form-group">
							<label for="email" class="col-md-3 control-label">Email</label>
							<div class="col-md-9">
								<form:input path="email" cssClass="form-control" />
								<form:errors path="email" cssClass="text-danger" />
							</div>
						</div>

						<div class="form-group">
							<!-- Button -->
							<div class="col-md-offset-3 col-md-9">
								<form:button class="btn btn-primary" >Salvar</form:button>
								<a href="${listagem}"
									class="btn btn-large btn-primary">Cancelar</a>
							</div>
						</div>
						
					</form:form>

				</div>
			</div>
		</div>
	</div>
</body>
</html>