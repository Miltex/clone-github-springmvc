package br.com.pifpaf.customer.service.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.google.gson.Gson;

import br.com.pifpaf.customer.model.Estado;
import br.com.pifpaf.customer.model.Region;
import br.com.pifpaf.customer.model.Regions;
import br.com.pifpaf.customer.service.EstatodosSerivce;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@Service
public class EstadosServiceImpl implements EstatodosSerivce {

	private static final String BASE_URL = "https://localhost:9002/pifpafws/rest/countries/BR";

	@Override
	public List<Region> obterEstados() throws IOException {
		// TODO pegar o xml realizar o parse.
		String resp = requestEstdos();
		
		String jsonString = convertXMLtoJSON(resp);
		
		Estado pojo = convertPojo(jsonString);
		
		return Arrays.asList(pojo.getCountry().getRegions().getRegion());
	}
	
	private Estado convertPojo(String json)  {
		Gson gson = new Gson();
		Estado staff = gson.fromJson(json, Estado.class);
		return staff;
	}
	
	private String convertXMLtoJSON(String xml) {
		String jsonPrettyPrintString = "";
		try {
            JSONObject xmlJSONObj = XML.toJSONObject(xml);
            jsonPrettyPrintString = xmlJSONObj.toString(4);
            System.out.println(jsonPrettyPrintString);
            
        } catch (JSONException je) {
            System.out.println(je.toString());
        }
		return jsonPrettyPrintString;
	}

	private String requestEstdos() throws IOException {
		String username = "admin";
		String password = "nimda";
		String credentials = username + ":" + password;
		OkHttpClient client = new OkHttpClient();

		
		final String basic = "Basic " + Base64.getEncoder().encodeToString(credentials.getBytes());

		Request request = new Request.Builder().url(BASE_URL).header("Authorization", basic).build();

		try (Response response = client.newCall(request).execute()) {
			return response.body().string();
		}
	}

}
