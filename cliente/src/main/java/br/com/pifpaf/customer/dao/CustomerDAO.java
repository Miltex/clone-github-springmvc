package br.com.pifpaf.customer.dao;

import java.util.List;

import br.com.pifpaf.customer.model.Customer;


public interface CustomerDAO {

    public List < Customer > getCustomers();

    public void saveCustomer(Customer theCustomer);

    public Customer getCustomer(int theId);

    public void deleteCustomer(int theId);
}
