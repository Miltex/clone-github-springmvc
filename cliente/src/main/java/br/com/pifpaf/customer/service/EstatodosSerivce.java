package br.com.pifpaf.customer.service;

import java.io.IOException;
import java.util.List;

import br.com.pifpaf.customer.model.Customer;
import br.com.pifpaf.customer.model.Estado;
import br.com.pifpaf.customer.model.Region;


public interface EstatodosSerivce {

    public List <Region> obterEstados() throws IOException;

}