/**
 * 
 */
package br.com.pifpaf.customer.model;

/**
 * @author miltonfilho
 *
 */


public class Country {
	private String comments;

	private String baseStores;

	private Regions regions;

	private String isocode;

	private String sealed;

	private String active;

	private String zones;

	private String uri;

	private String sapCode;

	private String modifiedtime;

	private String name;

	private String pk;

	private String creationtime;

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getBaseStores() {
		return baseStores;
	}

	public void setBaseStores(String baseStores) {
		this.baseStores = baseStores;
	}

	public Regions getRegions() {
		return regions;
	}

	public void setRegions(Regions regions) {
		this.regions = regions;
	}

	public String getIsocode() {
		return isocode;
	}

	public void setIsocode(String isocode) {
		this.isocode = isocode;
	}

	public String getSealed() {
		return sealed;
	}

	public void setSealed(String sealed) {
		this.sealed = sealed;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getZones() {
		return zones;
	}

	public void setZones(String zones) {
		this.zones = zones;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getSapCode() {
		return sapCode;
	}

	public void setSapCode(String sapCode) {
		this.sapCode = sapCode;
	}

	public String getModifiedtime() {
		return modifiedtime;
	}

	public void setModifiedtime(String modifiedtime) {
		this.modifiedtime = modifiedtime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPk() {
		return pk;
	}

	public void setPk(String pk) {
		this.pk = pk;
	}

	public String getCreationtime() {
		return creationtime;
	}

	public void setCreationtime(String creationtime) {
		this.creationtime = creationtime;
	}
}
