package br.com.pifpaf.customer.dao;

import br.com.pifpaf.customer.model.User;


public interface UserDao {

	User findByUserName(String username);
	
}
