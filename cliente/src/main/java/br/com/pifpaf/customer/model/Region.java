package br.com.pifpaf.customer.model;

public class Region {
	private String isocode;

	private String sealed;

	private String pk;

	private String uri;

	public String getIsocode() {
		return isocode;
	}

	public void setIsocode(String isocode) {
		this.isocode = isocode;
	}

	public String getSealed() {
		return sealed;
	}

	public void setSealed(String sealed) {
		this.sealed = sealed;
	}

	public String getPk() {
		return pk;
	}

	public void setPk(String pk) {
		this.pk = pk;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
}
