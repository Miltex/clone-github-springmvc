package br.com.pifpaf.customer.model;

public class Regions {
	
	private Region[] region;

	public Region[] getRegion() {
		return region;
	}

	public void setRegion(Region[] region) {
		this.region = region;
	}
}
