/**
 * 
 */
package br.com.pifpaf.customer.controller;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.pifpaf.customer.model.Customer;
import br.com.pifpaf.customer.model.Estado;
import br.com.pifpaf.customer.model.Region;
import br.com.pifpaf.customer.service.CustomerService;
import br.com.pifpaf.customer.service.EstatodosSerivce;
import br.com.pifpaf.customer.service.impl.EstadosServiceImpl;

@Controller
@RequestMapping("/estados")
public class EstadosController {

    @Autowired
    private EstatodosSerivce estadosService;

    @GetMapping("/list")
    public String listCustomers(Model theModel) {
        List<Region> estados;
		try {
			estados = estadosService.obterEstados();
			theModel.addAttribute("estados", estados);
	        
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "list-estados";
    }

}
