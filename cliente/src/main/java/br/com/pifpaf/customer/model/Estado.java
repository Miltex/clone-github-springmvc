/**
 * 
 */
package br.com.pifpaf.customer.model;

/**
 * @author miltonfilho
 *
 */


public class Estado {
	
    private Country country;

    public Country getCountry ()
    {
        return country;
    }

    public void setCountry (Country country)
    {
        this.country = country;
    }

}
